import "./App.css";
import Zodiacs from "./components/Zodiacs";
import Months from "./components/Months";
import DaysOfWeek from "./components/DaysofWeeks";
import React from "react";
import Wrapper from "./UI/Wrapper";

function App() {
  return (
    <React.Fragment>
      <Wrapper>
        <Zodiacs />
      </Wrapper>
      <Wrapper>
        <Months />
      </Wrapper>
      <Wrapper>
        <DaysOfWeek />
      </Wrapper>
    </React.Fragment>
  );
}

export default App;
