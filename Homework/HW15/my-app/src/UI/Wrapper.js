import classes from "./Wrapper.module.css";
import React from "react";

const Wrapper = (props) => {
  return (
    <div className={classes.container}>
      <div className={classes.wrapper}>{props.children}</div>
    </div>
  );
};

export default Wrapper;
