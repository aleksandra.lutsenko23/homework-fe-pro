import Card from "../UI/Card";

const DaysOfWeek = () => {
  const week = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
  ];

  return (
    <div>
      <h2>Days of the week:</h2>
      <ul>
        {week.map((day) => (
          <Card>
            <li>{day}</li>
          </Card>
        ))}
      </ul>
    </div>
  );
};

export default DaysOfWeek;
