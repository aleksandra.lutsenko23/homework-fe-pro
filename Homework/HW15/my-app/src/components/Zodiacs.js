import Card from "../UI/Card";

const Zodiacs = () => {
  const zodiacsSigns = [
    "Aries",
    "Taurus",
    "Gemini",
    "Cancer",
    "Leo",
    "Virgo",
    "Libra",
    "Scorpio",
    "Sagittarius",
    "Capricorn",
    "Aquarius",
    "Pisces",
  ];

  return (
    <div>
      <h2>Zodiacs:</h2>
      <ul>
        {zodiacsSigns.map((sign) => (
          <Card>
            <li>{sign}</li>
          </Card>
        ))}
      </ul>
    </div>
  );
};

export default Zodiacs;
