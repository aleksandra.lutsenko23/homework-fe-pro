import Card from "../UI/Card";

const Months = () => {
  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  return (
    <div>
      <h2>Months:</h2>
      <ul>
        {months.map((month) => (
          <Card>
            <li>{month}</li>
          </Card>
        ))}
      </ul>
    </div>
  );
};

export default Months;
