// Створіть програму секундомір.

// * Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
// * При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий
// * Виведення лічильників у форматі ЧЧ:ММ:СС
// * Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції

//Task 1 Create a timer.

const startButton = document.getElementById("start");
const stopButton = document.getElementById("stop");
const resetButton = document.getElementById("reset");
const display = document.querySelector(".stopwatch-display");
const hours = document.querySelector(".hours");
const minutes = document.querySelector(".minutes");
const seconds = document.querySelector(".seconds");

const helperStyles = (add, remove) => {
  display.classList.remove(remove);
  display.classList.add(add);
};

let intervalHandler, minutesInterval, hoursInterval;
let second = 0,
  minute = 0,
  hour = 0;

const startTimerHandler = (e) => {
  helperStyles("green", "default");

  const counterHelper = (el, count = 0) => {
    count++;
    count <= 9 ? (el.innerText = `${"0" + count}`) : (el.innerText = count);

    console.log(count);

    if (count >= 60) {
      count = 0;
      count++;
    }
  };

  /////Можете подивитись чому не працює допоміжна функція counterHelper , намагалась зробити рефакторинг ,
  ///але чомусь каунтер не апдейтиться, не можу зрозуміти чому. Без рефакторингу все працює

  const countSec = () => {
    second <= 9
      ? (seconds.innerText = `${"0" + second}`)
      : (seconds.innerText = second);
    second++;
    console.log(second);

    if (second >= 60) {
      second = 0;
      second++;
    }
    //   counterHelper(seconds, second);
  };

  const countMins = () => {
    minute++;
    minute <= 9
      ? (minutes.innerText = `${"0" + minute}`)
      : (minutes.innerText = minute);

    console.log(minute);

    if (minute >= 60) {
      minute = 0;
      minute++;
    }
    //   counterHelper(minutes, minute);
  };

  const countHours = () => {
    counterHelper(hours, hour);
  };

  intervalHandler = setInterval(countSec, 1000);
  minutesInterval = setInterval(countMins, 60000);
  hoursInterval = setInterval(countHours, 3600000);
};

const stopTimerHandler = (e) => {
  helperStyles("red", "green");
  clearInterval(intervalHandler, minutesInterval, hoursInterval);
};

const resetTimerHandler = (e) => {
  helperStyles("silver", "red");
  (second = 0), (minute = 0), (hour = 0);
  seconds.innerText = `${"0" + second}`;
  minutes.innerText = `${"0" + minute}`;
  hours.innerText = `${"0" + hour}`;
};

startButton.addEventListener("click", startTimerHandler);
stopButton.addEventListener("click", stopTimerHandler);
resetButton.addEventListener("click", resetTimerHandler);
