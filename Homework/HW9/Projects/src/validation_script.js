// Реалізуйте програму перевірки телефону
// * Використовуючи JS Створіть поле для введення телефону та кнопку збереження<br>
// * Користувач повинен ввести номер телефону у форматі 000-000-00-00

// * Після того як користувач натискає кнопку зберегти перевірте правильність введення номера, якщо номер
// правильний
// зробіть зелене тло і використовуючи document.location переведіть користувача на сторінку
// https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg
// якщо буде помилка, відобразіть її в діві до input.

let number = document.querySelector(".number");
let input = document.createElement("input");
input.setAttribute(`placeholder`, `000-000-00-00`);
const pattern = /^\d{3}-\d{3}-\d{2}-\d{2}$/;
number.append(input);
let send = document.createElement("button");
input.after(send);
send.textContent = "SEND";

send.onclick = () => {
  if (pattern.test(input.value)) {
    input.classList.add("green");
    document.location =
      "https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg";
  } else {
    let error = document.createElement("div");
    const mis = document.createElement("p");
    mis.textContent = "ERROR";
    document.body.prepend(mis);
    mis.classList.add("red");
    let del = () => {
      mis.remove();
    };
    setTimeout(del, 2000);
  }
};
