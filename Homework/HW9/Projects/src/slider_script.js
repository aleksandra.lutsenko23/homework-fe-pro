// Слайдер
// Створіть слайдер кожні 3 сек змінюватиме зображення
// Зображення для відображення
// https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg<br>
// https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg<br>
// https://naukatv.ru/upload/files/shutterstock_418733752.jpg<br>
// https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg<br>
// https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg

// Task 3. Create a slider

const slides = document.querySelectorAll(".slide");
const btnLeft = document.querySelector(".slider__btn--left");
const btnRight = document.querySelector(".slider__btn--right");
const slider = document.querySelector(".slider");

let curSlide = 0;
const maxSlide = slides.length;

// slides.forEach((s, i) => (s.style.transform = `translateX(${100 * i}%)`));
//   0%, 100%,200%,300%

const goToSlide = function (slide) {
  slides.forEach(
    (s, i) => (s.style.transform = `translateX(${100 * (i - slide)}%)`)
  );
};

goToSlide(0);

//next slide

const nextSlide = function () {
  if (curSlide === maxSlide - 1) {
    curSlide = 0;
  } else {
    curSlide++;
  }
  goToSlide(curSlide); //-refactoring the code

  slides.forEach(
    (s, i) => (s.style.transform = `translateX(${100 * (i - curSlide)}%)`)
  );
  // -100%, 0%, 100%, 200%
};

const prevSlide = function () {
  if (curSlide === 0) {
    curSlide = maxSlide;
  } else {
    curSlide--;
  }

  goToSlide(curSlide);
};

btnRight.addEventListener("click", nextSlide);

btnLeft.addEventListener("click", prevSlide);

//play slideshow

setInterval(nextSlide, 3000);
