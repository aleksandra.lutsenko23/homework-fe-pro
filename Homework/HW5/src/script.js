//Создать объект "Документ", в котором определить свойства "Заголовок, тело, футер, дата".
//Создать в объекте вложенный объект - "Приложение".
//Создать в объекте "Приложение", вложенные объекты, "Заголовок, тело, футер, дата".
//Создать методы для заполнения и отображения документа.</li>

const document1 = {
  header: "<img src='Images/Roof.gif'/><br/>",
  body: "<img src='Images/FirstFloor.gif'/><br/>",
  footer: "<img src='Images/SecondFloor.gif'/><br/>",
  data: "<img src='Images/Basement.gif'/><br/>",

  application: {
    header: {
      info: prompt("Input the header"),
    },
    body: {
      info: prompt("Input the body"),
    },
    footer: {
      info: prompt("Input the footer"),
    },
    date: {
      info: new Date(),
    },
    print() {
      document.write(document1.application.header.info + "<br>");
      document.write(document1.application.body.info + "<br>");
      document.write(document1.application.footer.info + "<br>");
      document.write(document1.application.date.info + "<br>");
    },
  },
  show() {
    for (let el in document1) {
      document.write(document1[el]);
    }
  },
};

document1.application.print();
document1.show();
