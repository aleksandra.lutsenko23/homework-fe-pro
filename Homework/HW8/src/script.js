/*  Нарисовать на странице круг используя параметры, которые введет пользователь.<

 При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг". Данная кнопка должна являться единственным контентом в теле HTML документа, весь остальной контент должен быть создан и добавлен на страницу с помощью Javascript<
При нажатии на кнопку "Нарисовать круг" показывать одно поле ввода - диаметр круга. При нажатии на кнопку "Нарисовать" создать на странице 100 кругов (10*10) случайного цвета. При клике на конкретный круг - этот круг должен исчезать, при этом пустое место заполняться, то есть все остальные круги сдвигаются влево.*/

window.onload = () => {
  const button = document.querySelector(".button");

  const showFields = () => {
    let input = document.createElement("input");
    button.replaceWith(input);
    input.classList.add("input");

    let drawCircle = document.createElement("button");
    drawCircle.textContent = "Draw circles";
    drawCircle.classList.add("circles");
    input.after(drawCircle);

    drawCircle.onclick = (e) => {
      const diametr = input.value;

      // create 100 circles with provided diameter

      for (i = 0; i < 100; i++) {
        const div = document.createElement("div");
        document.body.append(div);
        div.style.width = `${parseFloat(diametr)}px`;
        div.style.height = `${parseFloat(diametr)}px`;
        div.style.background = `hsl(${Math.floor(
          Math.random() * 360
        )}, 50%, 50%)`;
        div.style.borderRadius = "50%";
        div.style.display = "inline-block";

        //remove element
        div.onclick = () => {
          div.remove();
        };
      }
    };
  };

  button.addEventListener("click", showFields);
};
