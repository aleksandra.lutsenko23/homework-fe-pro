const calculator = {
  displayValue: "0",
  firstOperand: null,
  waitingForSecondOperand: false,
  operator: null,
  memVal: 0,
};

// take the input user valu and update the screen to display it

function updateDisplay() {
  const display = document.querySelector(".calculator-screen");

  display.value = calculator.displayValue;
}

updateDisplay();

const keys = document.querySelector(".keys");
const mrc = document.querySelector(".mrc");

keys.addEventListener("click", function (event) {
  // Access the clicked element
  const target = event.target; //the same ass const { target } = event;

  console.log(target);

  // Check if the clicked element is a button.
  // If not, exit from the function
  if (!target.matches("button")) {
    return;
  }

  if (target.classList.contains("operator")) {
    handleOperator(target.value);
    updateDisplay();
    return;
  }

  if (target.classList.contains("decimal")) {
    inputDecimal(target.value);
    updateDisplay();
    return;
  }

  if (target.classList.contains("all-clear")) {
    resetCalculator();
    updateDisplay();
    return;
  }

  //calculator.displayValue += target.value;
  inputDigit(target.value);
  updateDisplay();

  if (target.classList.contains("m+")) {
    calculator.memVal = +calculator.displayValue;
    console.log(calculator.memVal);
  }

  if (target.classList.contains("mrc")) {
    calculator.displayValue = +calculator.memVal;
    updateDisplay();
  }

  if (target.classList.contains("m-")) {
    calculator.displayValue = -calculator.memVal;
    updateDisplay();
  }
});

function inputDigit(digit) {
  if (calculator.waitingForSecondOperand === true) {
    calculator.displayValue = digit;
    calculator.waitingForSecondOperand = false;
  } else {
    calculator.displayValue =
      calculator.displayValue === "0"
        ? digit
        : (calculator.displayValue += digit);
  }
  console.log(calculator);
}

function inputDecimal(dot) {
  if (!calculator.displayValue.includes(dot)) {
    calculator.displayValue += dot;
  }
}

function handleOperator(nextOperator) {
  const { firstOperand, displayValue, operator } = calculator;

  //converting string to number
  const inputValue = parseFloat(displayValue);

  if (firstOperand === null && !isNaN(inputValue)) {
    calculator.firstOperand = inputValue;
  } else if (operator) {
    const result = calculate(firstOperand, inputValue, operator);
    calculator.displayValue = String(result);
    calculator.firstOperand = result;
  }

  calculator.waitingForSecondOperand = true;
  calculator.operator = nextOperator;
  console.log(calculator);
}

function saveValue(value) {
  calculator.memVal = value;
  console.log(value);
}

function calculate(firstOperand, secondOperand, operator) {
  if (operator === "+") {
    return firstOperand + secondOperand;
  } else if (operator === "-") {
    return firstOperand - secondOperand;
  } else if (operator === "/") {
    return firstOperand / secondOperand;
  } else if (operator === "*") {
    return firstOperand * secondOperand;
  }

  return secondOperand;
}

function resetCalculator() {
  calculator.displayValue = "0";
  calculator.firstOperand = null;
  calculator.waitingForSecondOperand = false;
  calculator.operator = null;
}
