// Створи клас, який буде створювати користувачів з ім'ям та прізвищем. Додати до класу метод для виведення імені та прізвища

class Person {
  constructor(firstName, lastName) {
    (this.firstName = firstName), (this.lastName = lastName);
  }

  print() {
    const p = document.createElement("p");

    document.body.prepend(p);
    p.style.color = "violet";
    p.style.fontSize = "2rem";
    p.innerHTML = `Hi. My name is ${this.firstName} ${this.lastName}!`;
  }
}

const eleven = new Person("El", "No");
eleven.print();

// Створи список, що складається з 4 аркушів. Використовуючи джс зверніся до 2 li і з використанням навігації по DOM дай 1 елементу синій фон, а 3 червоний

const ul = document.createElement("ul");
const li1 = document.createElement("li");
const li2 = document.createElement("li");
const li3 = document.createElement("li");
const li4 = document.createElement("li");

document.body.append(ul);
ul.append(li1, li2, li3, li4);

li2.previousElementSibling.style.color = "blue";
li2.nextElementSibling.style.color = "red";

// Створи див висотою 400 пікселів і додай на нього подію наведення мишки. При наведенні мишки виведіть текст координати, де знаходиться курсор мишки

const div = document.createElement("div");
document.body.append(div);

div.style.height = "400px";
div.style.backgroundColor = "green";
div.style.width = "400px";
div.style.border = "1px solid #000000";
div.style.textAlign = "center";

const displayCoords = (e) => {
  div.innerHTML = ` Coordinates- X: ${e.clientX}, Y: ${e.clientY}`;
};

div.addEventListener("mousemove", displayCoords);

// Створи кнопки 1,2,3,4 і при натисканні на кнопку виведи інформацію про те, яка кнопка була натиснута

const div2 = document.createElement("div");
const button1 = document.createElement("button");
const button2 = document.createElement("button");
const button3 = document.createElement("button");
const button4 = document.createElement("button");

document.body.append(div2);
div2.append(button1, button2, button3, button4);

div2.style.marginTop = "50px";
button1.innerHTML = "button 1";
button2.innerHTML = "button 2";
button3.innerHTML = "button 3";
button4.innerHTML = "button 4";

const getClickedButton = (e) => {
  const p2 = document.createElement("p");
  div2.after(p2);

  console.log(e.target);
  const btnClicked = e.target.innerHTML;

  p2.innerHTML = btnClicked;
};

div2.addEventListener("click", getClickedButton);

// Створи див і зроби так, щоб при наведенні на див див змінював своє положення на сторінці

const div3 = document.createElement("div");
document.body.append(div3);

div3.style.marginTop = "100px";
div3.style.height = "100px";
div3.style.backgroundColor = "yellow";
div3.style.width = "100px";
div3.style.border = "1px solid #000000";

const moveOnMouse = (e) => {
  div3.style.transform = "translate3d(12px, 50%, 3em)";
};

div3.addEventListener("mouseover", moveOnMouse);

// Створи поле для введення кольору, коли користувач вибере якийсь колір, зроби його фоном body

const input = document.createElement("input");
const submit = document.createElement("button");

document.body.append(input);
document.body.append(submit);

input.style.marginTop = "50px";
submit.innerHTML = "Submit Color";
submit.style.marginLeft = "20px";
input.setAttribute("type", "color");

submit.addEventListener("click", function () {
  const color = input.value;
  console.log(input.value);
  document.body.style.backgroundColor = color;
});

// Створи інпут для введення логіну, коли користувач почне вводити дані в інпут виводь їх в консоль

const login = document.createElement("input");

document.body.append(login);
login.style.marginTop = "100px";
login.style.marginLeft = "100px";

login.addEventListener("keypress", function (e) {
  console.log(e.key);
});

// Створіть поле для введення даних у полі введення даних виведіть текст під полем

const input2 = document.createElement("input");
document.body.append(input2);

input2.setAttribute("placeholder", "Login");
input2.style.marginTop = "20px";
input2.style.display = "block";
input2.style.marginLeft = "10px";
const text = document.createElement("span");
text.style.marginLeft = "10px";

input2.after(text);

input2.addEventListener("change", () => {
  text.append(`${input2.value} \n`);
  console.log(text);
});
