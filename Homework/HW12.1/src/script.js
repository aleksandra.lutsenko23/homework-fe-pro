let [...buttons] = document.querySelectorAll(".btn");
let pressed = false;

buttons.forEach((element) => {
  document.addEventListener("keypress", (e) => {
    if (e.key.toUpperCase() === element.textContent.toUpperCase()) {
      if (!pressed) {
        element.classList.add("active");
        press = true;
      } else {
        element.classList.remove("active");
        press = false;
      }
    }
  });
});
