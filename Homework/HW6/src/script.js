// Task 1

class Human {
  constructor(name, surname, age) {
    this.name = name;
    this.surname = surname;
    this.age = age;
  }

  show() {
    document.write(
      `Name:${this.name}, Last Name: ${this.surname}, Age: ${this.age} </br>`
    );
  }
}
const users = [
  new Human("Yulia", "Shatybelko", 31),
  new Human("Sasha", "Lutsenko", 29),
  new Human("Kate", "Tatarchenko", 27),
];

let arrOfAges = [];

const sortAges = (users) => {
  for (let user in users) {
    const arr = users[user].age;
    arrOfAges.push(arr);
    arrOfAges.sort((a, b) => a - b);
  }

  return arrOfAges;
};

for (let user in users) {
  users[user].show();
}

document.write(sortAges(users));

// Task 2

class Human2 {
  constructor(name, surname, age, number, color) {
    this.name = name;
    this.surname = surname;
    this.age = age;
    this.number = number;
    this.color = color;
  }

  displayDetails() {
    document.write(
      `I'm ${this.name} ${this.surname}. My favourite color is ${this.color}</br>`
    );
  }
}

Human2.quiz = function () {
  const randomNumber = Math.floor(Math.random() * 9);
  console.log(randomNumber);

  const winner = users2.filter((user) => user.number === randomNumber);

  winner.length >= 1
    ? console.log(`We got a winner - ${winner[0].name}`)
    : console.log("We got no winner yet!");
};

const users2 = [
  new Human2("Yulia", "Shatybelko", 31, 4, "red"),
  new Human2("Sasha", "Lutsenko", 29, 7, "blue"),
  new Human2("Kate", "Tatarchenko", 27, 3, "yellow"),
];

for (let user in users2) {
  users2[user].displayDetails();
}

Human2.quiz();
