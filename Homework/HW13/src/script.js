// Выполнить запрос на https://swapi.dev/api/people получить список героев звездных войн.

//             Вывести каждого героя отдельной карточкой с указанием. Имени, половой принадлежности, рост, цвет кожи,
//             год рождения и планету на которой родился.

//             Создайте кнопку сохранить на каждой карточке. При нажатии кнопки записшите информацию в браузере

//             Возможные свойства  :
//             name строка - Имя этого человека.
//             birth_year строка - Год рождения человека в соответствии со вселенскими стандартами ДБЯ или ПБЯ - до битвы при Явине или после битвы при Явине. Битва при Явине - это битва, которая происходит в конце эпизода IV «Звездных войн»: Новая надежда.
//             eye_color строка - Цвет глаз этого человека. Будет "неизвестно", если неизвестно, или "н / д", если у человека нет глаза.
//             gender строка - Пол этого человека. Либо «Мужской», «Женский», либо «Неизвестный», «н / д», если у человека нет пола.
//             hair_color строка - Цвет волос этого человека. Будет "неизвестно", если неизвестно, или "н / п", если у человека нет волос.
//             height строка - Рост человека в сантиметрах.
//             mass строка - Масса человека в килограммах.
//             skin_color строка - Цвет кожи этого человека.
//             homeworld строка - URL ресурса планеты, планеты, на которой этот человек родился или населяет.
//             films array - массив URL-адресов киноресурсов, в которых был этот человек.
//             species array - массив URL-адресов ресурсов видов, к которым принадлежит этот человек.
//             starships array - Массив URL-адресов ресурсов звездолета, которые пилотировал этот человек.
//             vehicles array - массив URL-адресов ресурсов транспортного средства, которые пилотировал этот человек.
//             url строка - URL-адрес гипермедиа этого ресурса.
//             created строка - формат даты ISO 8601 времени создания этого ресурса.
//             edited строка - формат даты ISO 8601 времени, когда этот ресурс редактировался.

//             Для создания карточек испозуйте классы

const characterContainer = document.querySelector(".card");

localStorage.user = JSON.stringify([]);
let savedChar = [];

class Character {
  constructor(name, gender, height, skin_color, birth_year) {
    this.name = name;
    this.gender = gender;
    this.height = height;
    this.skin_color = skin_color;
    this.birth_year = birth_year;
  }

  renderCharacter(data) {
    const html = ` <article class="character">
    <div class="character__data">
      <h3 class="character__name">${data.name}</h3>
      <h4 class="character__region">${data.gender}</h4>
      <p class="character__row"><span>📏</span>${data.height}</p>
      <p class="character__row"><span>🖖</span>${data.skin_color}</p>
      <p class="character__row"><span>📆</span>${data.birth_year}</p>
      <input type="button" value="Save Character" id=${data.name}>
    </div>
  </article>`;
    characterContainer.insertAdjacentHTML("beforeend", html);
  }
}

const getJSON = function (url) {
  return fetch(url).then((response) => {
    if (!response.ok)
      throw new Error(`Character not found(${response.status})`);
    return response.json();
  });
};

const getCharactersData = async () => {
  await getJSON(`https://swapi.dev/api/people`)
    .then((data) => {
      console.log(data);
      data.results.map((char) => {
        const Characters = new Character(
          char.name,
          char.gender,
          char.height,
          char.skin_color,
          char.birth_year
        );

        Characters.renderCharacter(Characters);
        savedChar.push(Characters);
      });
    })
    .catch((err) => console.error(`${err.message} 💥💥💥`));
};

getCharactersData();
console.log(savedChar);

characterContainer.addEventListener("click", (e) => {
  if (e.target.type === "button") {
    savedChar.forEach((el) => {
      if (el.name.includes(e.target.id)) {
        console.log(e.target.id);
        let a = JSON.parse(localStorage.user);
        a.push(el);

        localStorage.user = JSON.stringify(a);
      }
    });
  }
});
