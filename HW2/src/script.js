"use strict";

// 1 task
const a = 5;
const b = 7;
let result;

a + b < 4 ? (result = "Мало") : (result = "Много");

console.log(result);

// 2 task

let message;
const login = "Директор";

login == "Вася"
  ? (message = "Привет")
  : login == "Директор"
  ? (message = "Здравствуйте")
  : login == ""
  ? (message = "Нет логина")
  : (message = "");

console.log(message);

// 3 task

let c = 5;
let d = 10;
let total = 0;

while (c < d - 1) {
  c++;
  if (c % 2 !== 0) {
    console.log(c);
  }

  total += c; //30
}
console.log(total);

// 4 task

//прямоугольник
for (let i = 0; i < 6; i++) {
  for (let j = 0; j < 27; j++) {
    document.write("$");
  }

  document.write("<br/>");
}

document.write("<br/>");
document.write("<br/>");

// прямоугольный треугольник
for (let i = 0; i < 10; i++) {
  for (let j = 0; j < i; j++) {
    document.write("$");
  }

  document.write("<br/>");
}

document.write("<br/>");
document.write("<br/>");

// треугольник равносторонный пирамида
let n = "&nbsp";
for (let i = 0; i < 8; i++) {
  for (let k = 8; k > i - 1; k--) {
    document.write(n, n);
  }
  for (let j = 0; j < i + 1; j++) {
    document.write("$", n, n);
  }
  document.write("<br>");
}

document.write("<br>");
document.write("<br>");

// ромб
let space = 8;
let star = 1;
let line = 10;
for (let i = 0; i <= line; i++) {
  for (let j = 0; j <= space; j++) {
    document.write("&nbsp");
  }
  for (let k = 0; k < star; k++) {
    document.write("$");
  }
  space--;
  star++;
  if (i >= line / 2 && i <= line) {
    star = star - 2;
    space = space + 2;
  }
  document.write("<br>");
}
